## About Media Image Unsplash

This module provides Unsplash integration for Media.

### Unsplash API
This module is currently under development, and aims to allow users to add images from the Unsplash 
API as image media items to be used in their sites.

This module requires the core Media entity in 8.4.0+ as well as the Crop API Module.

NOTICE: A NORMAL API ACCESS ALLOWS 50 CALLS PER HOUR. THERE IS ONE CALL DONE FOR EACH SEARCH
ONE CALL DONE FOR EACH ADD, AND ONE CALL FOR EACH CLICK TO THE NEXT/PREV PAGE. PLEASE KEEP THIS IN MIND!

To gain Unsplash API credentials, visit https://unsplash.com/login and click the "Join" button below login.

After enabling the module, you must visit /admin/config/media/media_image_unsplash and enter your
API credentials. At this time, only the Access Key is used. Other fields are available for future use.

To add images, go to /admin/config/media/media_image_unsplash/search and type a key word in the 
search box and click search. Once the images load you will have a title and alt text, as well 
as add button below each image. Fill in the title/alt text and click the add image button and the 
image will be added as a media item that can be seen at /admin/content/media.

Maintainers:
 - Steven (Jake) Wilmes (@swilmes) https://www.drupal.org/u/swilmes

Slack channel: #drupal-media
