/**
 * @file
 */

(function ($, Drupal) {
  "use strict";

  Drupal.behaviors.UnsplashMediaEntity = {
    attach: function (context) {
      document.getElementById('unsplash-search-form').addEventListener('submit', function(e) {
        e.preventDefault(); //to prevent form submission
        $('.unsplash-overlay').addClass('active');
        const query = document.getElementById('unsplashKeyWord').value;
        $('.unsplash-pager-prev').attr('data-query', query);
        $('.unsplash-pager-next').attr('data-query', query);
        $('#unsplash-results').load('/admin/config/media/media_image_unsplash/results?query=' + query, function() {
          $('.unsplash-overlay').removeClass('active');
          $('.unsplash-image-add', context).once('unsplashAdd').click(function (e) {
            e.preventDefault();
            $('.unsplash-overlay').addClass('active');
            var id = $(this).data('id');
            var title = document.getElementById(id + '-title').value ;
            var alt = document.getElementById(id + '-alt').value ;
            $.ajax('/admin/config/media/media_image_unsplash/add?id=' + id + '&title=' + title + '&alt=' + alt)
              .done(function (data) {
                $('.unsplash-overlay').removeClass('active');
                $('.unsplash-image-result[data-id=' + id +']').addClass('success');
              })
              .fail(function (jqXHR, textStatus, errorThrown) {
                $('.unsplash-overlay').removeClass('active');
                $('.unsplash-image-result[data-id=' + id +']').addClass('error');
              });
          });

          // FIX NEXT AND PREV BUTTONS FOR NEXT CALL
          var prev = $('.unsplash-results-container').attr('data-prev');
          var next = $('.unsplash-results-container').attr('data-next');

          if (prev || next) {
            $('.unsplash-pager').addClass('active');
          } else {
            $('.unsplash-pager').removeClass('active');
          }
          if (prev) {
            $('.unsplash-pager-prev').addClass('active');
            $('.unsplash-pager-prev').attr('data-page', prev);
          }else {
            $('.unsplash-pager-prev').removeClass('active');
          }
          if (next) {
            $('.unsplash-pager-next').addClass('active');
            $('.unsplash-pager-next').attr('data-page', next);
          }else {
            $('.unsplash-pager-next').removeClass('active');
          }
        });
      });

      $('.unsplash-pager-item', context).once('unsplashNext').click(function (e) {
        e.preventDefault();
        $('.unsplash-overlay').addClass('active');
        var query = $(this).attr('data-query');
        var page = $(this).attr('data-page');
        $('#unsplash-results').load('/admin/config/media/media_image_unsplash/results?query=' + query + '&pager=' + page, function() {
          $('.unsplash-overlay').removeClass('active');
          $('.unsplash-image-add', context).once('unsplashAdd').click(function (e) {
            e.preventDefault();
            $('.unsplash-overlay').addClass('active');
            var id = $(this).data('id');
            var title = document.getElementById(id + '-title').value ;
            var alt = document.getElementById(id + '-alt').value ;
            $.ajax('/admin/config/media/media_image_unsplash/add?id=' + id + '&title=' + title + '&alt=' + alt)
              .done(function (data) {
                $('.unsplash-overlay').removeClass('active');
                $('.unsplash-image-result[data-id=' + id +']').addClass('success');
              })
              .fail(function (jqXHR, textStatus, errorThrown) {
                $('.unsplash-overlay').removeClass('active');
                $('.unsplash-image-result[data-id=' + id +']').addClass('error');
              });
          });

          // FIX NEXT AND PREV BUTTONS FOR NEXT CALL
          var prev = $('.unsplash-results-container').attr('data-prev');
          var next = $('.unsplash-results-container').attr('data-next');
          var query = $('.unsplash-results-container').attr('data-query');

          if (prev || next) {
            $('.unsplash-pager').addClass('active');
          } else {
            $('.unsplash-pager').removeClass('active');
          }
          if (prev) {
            $('.unsplash-pager-prev').attr('data-page', prev);
            $('.unsplash-pager-prev').addClass('active');
          }else {
            $('.unsplash-pager-prev').removeClass('active');
          }
          if (next) {
            $('.unsplash-pager-next').attr('data-page', next);
            $('.unsplash-pager-next').addClass('active');
          }else {
            $('.unsplash-pager-next').removeClass('active');
          }
        });
      });
    }
  };

})(jQuery, Drupal);
