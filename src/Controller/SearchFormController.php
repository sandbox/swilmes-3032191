<?php

namespace Drupal\media_image_unsplash\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use \Symfony\Component\HttpFoundation\Response;
use Drupal\media\Entity\Media;
use GuzzleHttp\Client;
use Drupal\media_image_unsplash\UnsplashEmbedFetcher;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Unsplash API settings for this site.
 *
 * TODO: In this file move code that calls \Drupal::httpClient() to use $this->httpclient as well as adding the
 * TODO: EntityTypeManager via dependency injection rather than calling Media::create
 */

class SearchFormController extends ControllerBase {

  /**
   * The unsplash fetcher.
   *
   * @var \Drupal\media_image_unsplash\UnsplashEmbedFetcher
   */
  protected $fetcher;

  /**
   * Guzzle client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\media_image_unsplash\UnsplashEmbedFetcher $fetcher
   *   Unsplash fetcher service.
   * @param \GuzzleHttp\Client $httpClient
   *   Guzzle client.
   */
  public function __construct(UnsplashEmbedFetcher $fetcher, Client $httpClient) {
    $this->fetcher = $fetcher;
    $this->httpClient = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function process() {
    $build['unsplash_search'] = \Drupal::service('plugin.manager.block')
      ->createInstance('media_image_unsplash_search')
      ->build();

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('media_image_unsplash.unsplash_embed_fetcher'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveImages() {
    $query = $_GET['query'];
    $page = isset($_GET['pager']) ? $_GET['pager'] : '1';
    if ($query) {
      $rows = [];
      $access_key = \Drupal::config('media_image_unsplash.settings')->get('access_key');
      $url = 'https://api.unsplash.com/search/photos?query=' . $query . '&page='. $page .'&per_page=30';

      try {
        $response = \Drupal::httpClient()->get($url, array('headers' => array('Authorization' => 'Client-ID ' . $access_key)));
        $data = json_decode($response->getBody(), TRUE);

        if (empty($data)) {
          $out = '<div class="empty">Empty Response</div>';
          $build = [
            '#markup' => $out,
          ];
        }
        else {
          $data['query'] = $query;
          $data['prev'] = NULL;
          $data['next'] = NULL;
          $i = 0;
          $crop_string = '?ixlib=rb-0.3.5&q=80&fit=crop&h=400&w=400';
          foreach ($data['results'] as $result) {
            $data['results'][$i]['urls']['sized'] = preg_replace('/\?.*/', '', $result['urls']['full']) . $crop_string;
            $i++;
          }
          if ($page > 1) {
            $data['prev'] = $page - 1;
          }

          if ($page < $data['total_pages']){
            $data['next'] = $page + 1;
          }

          // TODO: Attach the library here, versus in the twig template.
          $build = array(
            '#theme' => 'media_image_unsplash_results',
            '#rows' => $data,
            '#cache' => [
              'max-age' => 0
            ]
          );
        }
      } catch (RequestException $e) {
        $out = '<div class="empty">Exception Thrown</div>';
        $build = [
          '#markup' => $out,
        ];
      }
    }
    else {
      $out = '<div class="empty">No Query Made</div>';
      $build = [
        '#markup' => $out,
      ];
    }
    // This is the important part, because will render only the TWIG template.
    return new Response(render($build));
  }

  /**
   * Save the image as a managed file and then create a media item that uses the image.
   */
  public function addMedia() {
    $id = $_GET['id'];
    $image = $this->fetcher->fetchUnsplashEmbed($id);
    $unsplash_folder = 'unsplash-images';
    $public_path = 'public://';
    $my_path = file_create_url("public://");
    $file = NULL;

    if (isset($image['urls']['full'])) {
      $local_uri = $public_path . $unsplash_folder . '/' . $id . '.' . pathinfo(parse_url($image['urls']['full'], PHP_URL_PATH), PATHINFO_EXTENSION) . 'png';
      $thumb_uri = $public_path . 'styles/thumbnail/public/media-icons/generic/' . $id . '.' . pathinfo(parse_url($image['urls']['thumb'], PHP_URL_PATH), PATHINFO_EXTENSION) . 'png';

    }
    $directory = dirname($thumb_uri);
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    $response = $this->httpClient->get($image['urls']['thumb']);
    if ($response->getStatusCode() == 200) {
      $file = file_unmanaged_save_data($response->getBody(), $thumb_uri, FILE_EXISTS_REPLACE);
    }

    $fileItem = \Drupal\file\Entity\File::Create([
      'uri' => $file,
    ]);
    $fileItem->save();

    $directory = dirname($local_uri);
    file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

    $response = $this->httpClient->get($image['urls']['full']);
    if ($response->getStatusCode() == 200) {
      $file = file_unmanaged_save_data($response->getBody(), $local_uri, FILE_EXISTS_REPLACE);
    }

    $fileItem = \Drupal\file\Entity\File::Create([
      'uri' => $file,
    ]);
    $fileItem->save();

    if ($fileItem) {
      $values = [
        'bundle' => 'image',
        'field_media_image' => [
          'target_id' => $fileItem->id(),
        ],
      ];

      $title = $_GET['title'];
      if ($title) {
        $values['name'] = $title;
      }

      $alt = $_GET['alt'];
      if ($alt) {
        $values['field_media_image']['alt'] = $alt;
      }

      $imageEntity = Media::create($values);
      $imageEntity->save();

      $response_array = ['status' => 'success', 'data' => ''];
    }
    else {
      $response_array = ['status' => 'fail', 'data' => ''];
    }

    $response = new Response(json_encode($response_array));
    $response->headers->set('Content-Type', 'application/json; charset=utf-8');
    return $response;
  }
}
