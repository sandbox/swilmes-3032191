<?php

namespace Drupal\media_image_unsplash\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configures Unsplash API settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_image_unsplash_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['media_image_unsplash.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['application_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application ID'),
      '#description' => $this->t('Unsplash app id'),
      '#default_value' => $this->config('media_image_unsplash.settings')->get('application_id'),
    ];
    $form['access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key'),
      '#description' => $this->t('Unsplash access key'),
      '#default_value' => $this->config('media_image_unsplash.settings')->get('access_key'),
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#description' => $this->t('Unsplash secret key'),
      '#default_value' => $this->config('media_image_unsplash.settings')->get('secret_key'),
    ];

    $form['callback_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback URL'),
      '#description' => $this->t('Unsplash callback url'),
      '#default_value' => $this->config('media_image_unsplash.settings')->get('callback_url'),
    ];
    $form['callback_url_auth'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Callback URL Authorization'),
      '#description' => $this->t('Unsplash callback url authorization token'),
      '#default_value' => $this->config('media_image_unsplash.settings')->get('callback_url_auth'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('media_image_unsplash.settings')
      ->set('application_id', $values['application_id'])
      ->set('access_key', $values['access_key'])
      ->set('secret_key', $values['secret_key'])
      ->set('callback_url', $values['callback_url'])
      ->set('callback_url_auth', $values['callback_url_auth'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
