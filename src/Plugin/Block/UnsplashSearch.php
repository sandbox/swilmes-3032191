<?php

namespace Drupal\media_image_unsplash\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\views\Views;

/**
 * Provides a block for the searching of Unsplash for images.
 *
 * @Block(
 *   id = "media_image_unsplash_search",
 *   admin_label = @Translation("Unsplash Search Block")
 * )
 */
class UnsplashSearch extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $path = '/' . drupal_get_path('module', 'media_image_unsplash');

    return [
      '#theme' => 'media_image_unsplash_search',
      '#rows' => null,
      '#module_path' => $path,
      '#cache' => [
        'max-age' => 0
      ]
    ];
  }

}
