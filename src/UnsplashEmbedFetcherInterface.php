<?php

namespace Drupal\media_image_unsplash;

/**
 * Defines a wrapper around the Unsplash oEmbed call.
 */
interface UnsplashEmbedFetcherInterface {

  /**
   * Retrieves a unsplash posts by search term.
   *
   * @param int $shortcode
   *   The unsplash query term.
   *
   * @return array
   *   The unsplash information.
   */
  public function fetchUnsplashEmbed($shortcode);

}
